﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Npgsql;
using TestAPI.Models;
using Dapper;

namespace TestAPI.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private static string connstr = @"Host=103.106.81.54; 
                 port=5432;Database=testing_core;
                 user id=postgres;password=Password!234";

        [Route("getUser")]
        public IEnumerable<TabelUser> GetUser()
        {
            NpgsqlConnection connection = new NpgsqlConnection(connstr);
            connection.Open();
            var data = connection.Query<TabelUser>(@"SELECT user_id, 
                user_name,
                email,
                full_name,
                password
                FROM tr_user");
            connection.Close();
            return data;
        }

        
        public int InsertUser(TabelUser tabelUser)
        {

            NpgsqlConnection connection = new NpgsqlConnection(connstr);
            string sqlInsert = @"INSERT INTO tr_user(user_name,
                full_name,email,password)
                VALUES(@user_name,@full_name,@email,@password)";
            var insert = connection.Execute(sqlInsert, new
            {
                user_name = tabelUser.user_name,
                full_name = tabelUser.full_name,
                email = tabelUser.email,
                password = tabelUser.password
            });
            return insert;
        }

        [Route("Insert")]
        [HttpPost]
        public string Create(TabelUser tabelUser)
        {

            string message;
            NpgsqlConnection connection = new NpgsqlConnection(connstr);
            try
            {
                string sqlInsert = @"INSERT INTO tr_user(user_name,
                full_name,email,password)
                VALUES(@user_name,@full_name,@email,@password)";
                var insert = connection.Execute(sqlInsert, new
                {
                    user_name = tabelUser.user_name,
                    full_name = tabelUser.full_name,
                    email = tabelUser.email,
                    password = tabelUser.password
                });

                 message = "Ok";
            }
            catch
            {
                 message = "NG";
            }
          
            return message;
        }

    }
}