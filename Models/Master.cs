﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAPI.Models
{
    public class TabelUser
    {
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string email { get; set; }
        public string full_name { get; set; }
        public string password { get; set; }
    }

    public class outputTableUser
    {
        public string result { get; set; }
        public string message { get; set; }
        public object User { get; set; }
    }
}
